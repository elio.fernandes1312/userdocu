# Animate Harmonic Results in Paraview

In this tutorial is a simple example used to show how to animate harmonic results in paraview. As Simulation we use the [cantilever application](../../../Applications/Singlefield/Mechanics/Cantilever/README.md) and visualize the deformation via `Warp by vector` and animate the harmonic displacement.

![](cantilever.gif)

1. Open CFS-file
2. Choose needed `Region`
3. Check **`Animate Harmonics/Modal Results`**
4. Hit `Apply`
5. Choose right `Analysis Step` 
6. Choose right `Frequency Step`
7. Hit `Apply` 

Now we want to visualize the displacement field, therefore:

8. Select `Warp by Vector` (to see some cool deformation)
9. Hit `Apply`
10. Select under Vectors **`mechDisplacement-mode`** (Its important to always select the **mode-result**, if choosen amplitude the direction is lost)
11. Select a good `Scale Factor`
12. Hit `Apply`
13. Press play and enjoy a nice animation

All the steps above are visualized as a gif as well:

![](ani.gif)


### Hint, Tipps and Tricks
* To get the animation smoother, you can change the frames per second and many more under _`View => Animation View`_ 
* You can also animate scalarfields (e.g. temperature field), vectorfields (e.g. magnetic flux density) and many more.


```python

```
