Common Visualiation Tasks
=========================

## Loading a CFS result file

1. In the *menu bar* go to *File → Open...* and then select the result file from CFS, e.g. `results_hdf5/run.cfs`
2. Mark the result in the Pipeline Browser and tick/untick the Regions you want to load in the *Properties tab*
3. click *Apply* in the Properties tab

## Visualizing Field Results

After opening a result file

1. Mark the result in the Pipeline Browser
2. Select which result to display in the drop-down of the *Active Variable Controls* toolbar
  >  The icon shows if results are defined on the nodes (points) or in the center of the elements (cell symbol).
3. Use the *Representation Toolbar* to choose the desired appearance
  >  Most useful are **Surface With Edges** to show element boundaries, or just **Surface**.
