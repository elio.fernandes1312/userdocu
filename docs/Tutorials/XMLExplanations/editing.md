# Editing an xml file with Eclipse
Let us consider the examplatory xml file [example.xml](example.xml).
Open it in eclipse and choose the "Source" view (per default it's in "Design" view) in the lower left region of the source-code display window.

Now you can left-click on any tag and on the right side of the display you can see the "Outline" window, where the current file is shown in a nested way.
Just right-click any tag and you can now choose to add a new attribute or children.
Or you can also work in the "Source" view at get the suggestions by opening a new tag with "<" or simply add a blank space after an attribute to get further suggestions

![a2](xmleclipse2.png)
*Outline Window, showing the nested xml structure*


## Validating the xml file
If you want to check the xml file for any errors, you have to add it to an xml-project, unfortunately it does not work any other way.
Go to *File->New->Project* and generate a new example xml project (store it at the default eclipse workspace location) 

![a3](newproject.png)
*Generate a new example xml project*


Then right-click on the newly created prject *XMLExamples* in the ProjectExplorer and choose *New->File*, check the box *Link to file in the file system* and select the xml-file you want to validate

![a4](examplefile.png)
*Add the xml file to the project*

Work is done, simply right click on the xml file and choose *Validate*, which shows you any errors in your xml file.


After clicking *Validate*, any occuring errors (spelling mistakes, inconsistencies with the xsd scheme,...) will be marked.

![a6](examplefile3.png)
*Errors in the xml file are indicated*

## XML-Autocompletion

If you have no autocompletion, probably the wrong xml-schema location is defined. Simply replace the header tag with the lines blow for the **material-xml**, **simulation-xml** and **cfsdat-xml**. 

### XML-Autocompletion if you have internet
* **Simulation-Input-XML**: Be sure that the header tag `<cfsSimulation ...>` looks like this:
```
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
```
* **Material-Input-XML**: Be sure that the header tag `<cfsMaterialDataBase ...>` looks like this:
```
<cfsMaterialDataBase xmlns="http://www.cfs++.org/material"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.cfs++.org/material 
https://opencfs.gitlab.io/cfs/xml/CFS-Material/CFS_Material.xsd" >
```
* **CFSDat-Input-XML**: Be sure that the header tag `<cfsdat ...>` looks like this:
```
<cfsdat xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Dat/CFS_Dat.xsd">
```

### XML-Autocompletion if you have no internet
If you have to work offline, you have to define where the schemas can be found. The schemas are automatically distributed with the cfs-installation and located under `localPathToCFS/cfs/share/xml/...`.
IF you do not know the installation path of openCFS you can simply type this terminal command (on ubuntu):
```
which cfs
```
* **Simulation-Input-XML**: Be sure that the header tag `<cfsSimulation ...>` looks like this. **Attention: You have to adapt to the path of your local cfs-installation**:
```
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
local/Path/to/cfs/share/xml/CFS-Simulation/CFS.xsd">
```
* **Material-Input-XML**: Be sure that the header tag `<cfsMaterialDataBase ...>` looks like this. **Attention: You have to adapt to the path of your local cfs-installation**:
```
<cfsMaterialDataBase xmlns="http://www.cfs++.org/material"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.cfs++.org/material 
local/Path/to/cfs/share/xml/CFS-Material/CFS_Material.xsd" >
```
* **CFSDat-Input-XML**: Be sure that the header tag `<cfsdat ...>` looks like this. **Attention: You have to adapt to the path of your local cfs-installation**:
```
<cfsdat xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
local/Path/to/cfs/share/xml/CFS-Dat/CFS_Dat.xsd">
```

## Scheme Suggestions
In case you don't know our openCFS xml-scheme by heart, eclipse's xml plugin helps you and suggests the appropriate tags and elements...if you ask for it.
How you do that, is described in the following.

For example let us consider we want to know which boundary conditions and loading options exist in a certain PDE.
Just navigate to the <bcsAndLoads> press Enter for a linebreak, open a new tag with the '<' symbol and wait a second.
Then you get the possible boundary conditions suggestions together a brief description.

![a7](xmleclipse3.png)

On the other hand it is also possible to display the element options inside a tag, therefore just navigate inside a tag (between < and > symbol), insert a blank space and press 'crtl+Space', wait a second and you will see the possible elements together with a brief description.

![a8](xmleclipse4.png)




