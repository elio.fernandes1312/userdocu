# Data Input and Output

The data processing tool of *openCFS* offers the option of importing *Ensight*-files and *hdf5*-files (hierachical data format), whereas the export of mesh based field data is by default based on *hdf5*, which is the native data format of *openCFS*. Additionally, reading of meshes (e.g. target mesh for interpolation) in *cgns* or *cdb* format is supported.  Field data can thereby be defined on the *nodes* or the *cell centroids* of a compuational grid in the *time* or the *frequency* domain.

## Data Input

The first block of the xml-scheme defines the time domain of the input data to be read. The following xml-snippet illustrates a typical setting.

```
 <stepValueDefinition>
	<startStop>
		<startStep value="0"/>
		<numSteps value="10"/>
		<startTime value="1e-05"/>
		<delta value="1e-05"/>
		<deleteOffset value="no"/>
	</startStop>
</stepValueDefinition>
```

* *delta*: time step size in seconds for data import (*cfs time step*. Thereby, the time step size can be a whole multiple of the time step provided by the imput file to read every 2nd, 3rd etc. time step. If for example the input data time step is 1s and the defined *cfs time step* is 2s, every second time step is read.
* *startStep*: time offset in multiples of the *cfs time step*.
* *numSteps*: number of time steps to be read.
* *startTime*: offset in seconds according to the input data time values.
* *deleteOffset*: delete the offset resulting from *startTime*. If set to *yes* and *startStep* to zero, the first time value of the output data will be the *cfs time step* size.

The first time step which is read from the input file is the time step corresponding to the time *startStep* * *delta* + *startTime* in seconds. Thereby, the offset resulting from *startTime* can be deleted for the output by enabling the *deleteOffset*-tag.
If data is processed in frequency domain, the same tags are used (*startTime* defines the start frequency in this case).

Subsequently, the mesh based input data is provided by

```
<meshInput id="input">
	<inputFile>
		<hdf5 fileName="pathToInputFile/InpuFile.hdf5"/>
	</inputFile>
</meshInput>
```            
in case of using the hfd5 format (e.g. *openCFS* simulation file).

*Ensight* data is considered in the xml-scheme by

```
<meshInput id="input" gridType="fullGrid">
	<inputFile>
		<ensight fileName="pathToInputFile/InputFile.case" fixFVPyramids="yes" readFVMesh="no">
			<variableList>
				<variable CFSVarName="cfsQuantity1" EnsightVarName="EnsightQuantity1"/>
				<variable CFSVarName="cfsQuantity2" EnsightVarName="EnsightQuantity2"/>
			</variableList>
		</ensight>
	</inputFile>
</meshInput>
```         
,where in *fileName* the location of the *Ensight* master file (.case or .encas) needs to be provided and the therein defined quantities (e.g velocity, pressure) need to be defined by *EnsightVarName*. In contrast, the quantities of *hdf5* files are identified automatically by *openCFS*.



## Data Output

The processed field data (*resultQuantity1* and *resultQuantitiy2*) in the follwoing xml-snippet) is exported in the native hdf5-format. Thereby, the filename is defined in the xml scheme and the default file extension *cfs* can be adapted, if required. Furthermore, the default compression level of the hdf5-file of 1 can be modified. Furthermore, external files can be enabled, where the field data of each time/frequency step is written to a seperate hdf file and the master file includes the mesh data, further file information, and the links to the external files of each time step. To explore hdf5 files and get an understanding of the structure, *HDFview* is recommended. The results of multiple filters (e.g. *filterID1*, *filterID2* in the following example xml) can be either written to all regions or to specified regions of the output mesh as indicated in the snippet.
```
<meshOutput id="OutputFileName" inputFilterIds="filterID1,filterID2">
	<outputFile>
		<hdf5 extension="cfs" compressionLevel="1" externalFiles="no"/>
	</outputFile>
	<saveResults>
		<result resultName="resultQuantity1">
			<allRegions/>
		</result>
		<result resultName="resultQuantity2">
			<regionList>
				<region name="region1"/>
				<region name="region2"/>
			</regionList>
		</result>
	</saveResults>
</meshOutput>
```

**Important:** If the exported data will be the input of a subsequent *openCFS* simulation, *openCFS* variable names must be used for the declaration of field quantitites. Thus, for the acoustic PDE, one of the following names must be chosen.

General acoustic and fluid mechanic quantities:

* *acouPressure*
* *acouVelocity*
* *acouPotential*
* *acoutIntensity*
* *fluidMechVelocity*
* *meanFluidMechVelocity*
* *fluidMechPressure*
* *fluidMechDensity*
* *fluidMechVorticity*
* *fluidMechGradPressure*


AeroacousticSourceTerms:

* *acouRhsLoad* (general)
* *acouRhsLoadP* (PCWE)
* *vortexRhsLoad* (Vortex Sound Theorie)
* *acouDivLighthillTensor* (Lighthill's acoustic analogy)

