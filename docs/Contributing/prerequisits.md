Prerequisits
============

To start contributing you need the following:

* [**Gitlab-account:**](#creating-a-gitlab-account)
You need to have a gitlab-account to get access to the [Userdocumentation-repository](https://gitlab.com/openCFS/userdocu)
* [**Git:**](#installing-git)
A version control program. It runs in the background of git and you can use it for obtaining the repostory, changing it and merge back to the original project (and many more).
* [**Anaconda/python:**](#installing-anaconda)
MkDocs needs pyhton and anaconda is open-source-distribution of python, which contains many helpful programs, for e.g. Jupyter-Notebook which can be used to create and edit markdown-files.
* [**MkDocs:**](#installing-mkdocs)
The enviroument on which the Userdocumention-Website is build. With mkDocs you can host your local server and see instantly the changes you make to the userdocumentation. This is a great way to see if everything works as intendet.

## Creating a Gitlab-account
Simply create a Gitlab-account [here](https://gitlab.com/users/sign_up).
### Create and add ssh-key to gitlab account
Add a ssh-key to your Gitlab-accaunt to enable automatic authentification (no longer annonying password requests).

* **Under Ubuntu:**
Follow this  [guide](https://gitlab.com/-/profile/keys) on gitlab.com
* **Under Windows:**
Follow these [instruction](http://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key). As terminal use the git bash.
If standard settings are used, your public ssh-key should be in `C:\Users\YourUsername\.ssh`.
The public key looks something like this: `key_name.pub` Open the public key with the texteditor, copy the key and add it to your [account](https://gitlab.com/-/profile/keys)

## Installing Git

[What is git?](https://www.coredna.com/blogs/what-is-git-and-github-part-two)

Here are some slides of the basic of git: [Git-Info-Slides](slides_git.pdf)

* **Under Ubuntu:**
Install git with the command: `# sudo apt install git`
* **Under Windows:**
Download git from http://git-scm/download/win and install it with standard settings. This installs the **git-bash**. In the following commands which have to be executed in the git bash be marked with a `G#` in front of the commando (eg.: as `G# git status`).

## Installing Anaconda
* **Under Ubuntu:**
https://docs.anaconda.com/anaconda/install/linux/
* **Under Windows:**
https://docs.anaconda.com/anaconda/install/windows/
Now you have also an **anaconda bash**. In the following commands which have to be executed in the git bash be marked with a `A#` in front of the commando (eg.: as `A# pip install mkdocs`).

## Installing MkDocs
You can install MkDocs (and all requirements) with the following code:
```
# pip install -r requirements.txt
```
Here is the [requirements.txt](https://gitlab.com/openCFS/userdocu/-/blob/master/requirements.txt).
For more details look [here](https://note.nkmk.me/en/python-pip-install-requirements/).
Keep in mind to use the correct terminal if you use windows.

Alternativly you can install each package on its own:

* **Under Ubuntu:**
Install following packages:
    - Mkdocs: `# pip install mkdocs `
    - LaTex rendering: `# pip install pymdown-extensions`
    - Bibtex bibliographs: `# pip install mkdocs-bibtex`
    - mkdocs-material: `# pip install mkdocs-material `
* **Under Windows:**
Open the Anaconda promt and execute following commandos:
    - Mkdocs: `A# pip install mkdocs`
    - LaTex rendering: `A# pip install pymdown-extensions`
    - Bibtex bibliographs: `A# pip install mkdocs-bibtex`
    - mkdocs-material: `A# pip install mkdocs-material`
